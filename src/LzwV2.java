

//Classe qui contient l'algorithme pour faire la compression
//et la decompression d'un fichier.
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

public class LzwV2  {
	// http://tutorials.jenkov.com/java-io/fileoutputstream.html
	// Utiliser pour amliorer la performance quand on crit dans le fichier. Voir URL.
	// Valeur optimal https://stackoverflow.com/questions/32172761/what-is-the-optimal-file-output-buffer-size
	int bufferSize = 4 * 1024;
	short dictMaxSize = Short.MAX_VALUE;
	private HashMap<String, Short> lzwStringDict;
	private HashMap<Short, String> lzwShortDict;
	private TimerCustom timer;
	static String fileNameInput = "C:\\Users\\User\\Desktop\\Compress\\ex.txt";
	static String fileNameOutput = "C:\\Users\\User\\Desktop\\Compress\\Lzex.txt";

	
	public LzwV2() {
		timer = new TimerCustom();
	}
	
	private void initializeStringDictionnary() {
		lzwStringDict = new HashMap<String, Short>();
		// Initialize the dictionary with the first 256 ASCII characters.
		for (int i = 0; i < 256; i ++) {
			lzwStringDict.put(Character.toString((char)i), (short) i);
			
			//System.out.println(Character.toString((char)i)+ " Short - = "+ (short) i);
		}
	}
	
	private void intializeShortDictionnary() {
		lzwShortDict = new HashMap<Short, String>();
		
		for (int i = 0; i < 256; i ++) {
			lzwShortDict.put((short)i, Character.toString((char)i));
			//System.out.println((short)i+" String = " + Character.toString((char)i));
		}
	}
	
	public void compress(String fileNameInput, String fileNameOutput) {
		initializeStringDictionnary();
		//Permet de connatre la performance du programme.
		timer.start();
		
		try (
				// http://tutorials.jenkov.com/java-io/fileoutputstream.html
				// En utilisant un BufferedInputStream, on vite de lire les bytes 1 par 1. Ce qui augment nettement la performance.
				InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);
	            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(fileNameOutput),bufferSize);
				DataOutputStream dataOutput = new DataOutputStream(outputStream);)
		{
				
				
         //Voir l'algo, jsuis pas capble de l'expliquer.
         String symbol = Character.toString((char)inputStream.read());
         System.out.println("symbol 1 " + symbol);
         int byteRead;
         String concat;
         //Lis le fichier jusqu'  la fin
         while ((byteRead = inputStream.read()) != -1) {
         	concat = symbol + Character.toString((char) byteRead);
         	
         	//Si le dictionnaire contient la concat nation du symbol et du dernier byte lue, on met la concat nation
         	//dans la variable "symbol" et on lie le prochain byte.
         	if(lzwStringDict.containsKey(concat)) {
         		symbol = concat;
         		
         	//Sinon, on ajoute la concat nation dans le dictionnaire et la variable "symbole" est reinitialiser pour ne contenir
         	//que la valeur du dernier byte lue. On  crit dans le fichier la valeur int de la concat nation dans le dictionnaire.
         	} else {
         		dataOutput.writeShort(lzwStringDict.get(symbol));
         		
         		lzwStringDict.put(concat,(short) lzwStringDict.size());
             	
         		//reset le dictionnaire quand il devient trop grand
         		if(lzwStringDict.size() >= dictMaxSize)
         		{
         			initializeStringDictionnary();
         		}
             	//Ajoute le dernier byte lue
             	symbol = Character.toString((char) byteRead);
         	}
         }
         dataOutput.writeShort(lzwStringDict.get(symbol.toString()));
         
	     } catch (IOException ex) {
	            ex.printStackTrace();
	     }
		
		timer.stop();
	}

	
	public void decompress(String fileNameInput, String fileNameOutput) {
		intializeShortDictionnary();
		timer.start();
		try (
				// http://tutorials.jenkov.com/java-io/fileoutputstream.html
				// En utilisant un BufferedInputStream, on  vite de lire les bytes 1 par 1. Ce qui augment nettement la performance.
				InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);
				DataInputStream dataInput = new DataInputStream(inputStream);
	            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(fileNameOutput), bufferSize);)	{
			
         
			String s = null;
         short key;
         String seq;
         
         //Lis le fichier jusqu'  la fin
         while (dataInput.available() > 0) {
         	key = dataInput.readShort();
         	
         	if (lzwShortDict.containsKey(key)) {
         		seq = lzwShortDict.get(key);
         	} else {
         		seq = s + s.substring(0,1);
         	} 
     		
     		for(char c : seq.toCharArray()) {
     			outputStream.write((int) c);
     		}
     		
         	if(s != null) {
         		lzwShortDict.put((short)lzwShortDict.size(),s + seq.substring(0, 1));
         		
         		//On reset le dictionnaire quand il devient trop grand
         		if(lzwShortDict.size() >= dictMaxSize)
         		{
         			intializeShortDictionnary();
         		}
         	}
         	
         	s = seq;
         }
         
	     } catch (IOException ex) {
	            ex.printStackTrace();
	     }
		timer.stop();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	LzwV2 lz = new LzwV2();
	lz.compress(fileNameInput, fileNameOutput);
	
	}
	
}
