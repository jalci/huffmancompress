package algorithCompression;

	import java.io.BufferedInputStream;
	import java.io.BufferedOutputStream;
	import java.io.DataOutputStream;
	import java.io.FileInputStream;
	import java.io.FileNotFoundException;
	import java.io.FileOutputStream;
	import java.io.IOException;
	import java.io.InputStream;
	import java.io.OutputStream;
	import java.util.ArrayList;
	import java.util.HashMap;
	import java.util.List;
	import java.util.PriorityQueue;
	import java.util.stream.IntStream;
	;
	public class HuffmanCompressorTest implements CompressionAlgorithm{



		int bufferSize = 20 * 1024;
		private TimerCustom timer;
		private int[] frequTable;
		private HashMap<Integer, String> codesMap;
		private HashMap<Integer, ArrayList<Integer>> intListMap;
		
		public HuffmanCompressorTest() {
			timer = new TimerCustom();
		    codesMap = new HashMap<Integer, String>();
		    intListMap = new HashMap<Integer, ArrayList<Integer>>();

		}

		
		@Override
		public void compress(String fileNameInput, String fileNameOutput) {
			timer.start();

			//Build frequency table from input file
			frequTable = buildFrequencyTable(fileNameInput);
			
			//Create minHeap with frequencies
			PriorityQueue<NodeElement> pqNode = new PriorityQueue<NodeElement>();
			buildMinHeap(frequTable, pqNode);
			
			
			
			//Create Huffman tree
		    // Phase 2  - Build Huffman Tree
			NodeElement root = buildHuffmanTree(pqNode);
		    
			//Build a look-up table for each character's code
				//Build Int - String map
		    buildCodeMap(frequTable, root, codesMap);
//		    System.err.println(" Old Table Codes ");
//		    for (Integer key : codesMap.keySet()) {
//				System.out.println("Key " + key + " Val " + codesMap.get(key));
//			}
		    
		    	//Build Int - ArrayList map - Because of trouble writing in output file (char vs int)
		    buildCodeMap2(frequTable, root,intListMap);
		    
//		    System.err.println(" New intList  Table Codes ");
//		    for (Integer key : intListMap.keySet()) {
//				String fullVal ="";
//				for (int val : intListMap.get(key)) {
//					fullVal+=Integer.toString(val);
//				}
//				System.out.println("Key " + key + " Values " + fullVal);
//			}
//			
			//Phase 3 - Encode data to file
			//OutPut file
			//encodeFile1(fileNameInput,fileNameOutput);
			//encodeFile(fileNameInput,fileNameOutput);
			//encodeFile2(fileNameInput,fileNameOutput);
		    
		    encodeFileFromListOfCodeMap(fileNameInput, fileNameOutput);
		    //encodeFromStringBinary(fileNameInput, fileNameOutput);
		    
		    timer.stop();
		}
		




		// ##### ----------------------------- Compression Methods  --------------------------------------------#####

		
		// Will build frequency table from input file
		private int[] buildFrequencyTable(String fileNameInput) {
			int[] freq = new int[256];

			try (
					// http://tutorials.jenkov.com/java-io/fileoutputstream.html
					// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce
					// qui augmente nettement la performance.
					InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);) {

				int byteRead;

				// Lis le fichier jusqu'� la fin
				while ((byteRead = inputStream.read()) != -1) {
					freq[byteRead] += 1;
				}

				// Fermer le lecteur
				inputStream.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			return freq;
		}

	 
			
		//From frequencies, we build a minHeap that we'll use to create Huffman tree
		private void buildMinHeap(int[] freq, PriorityQueue<NodeElement> pqueue) {

			for (int i = 0; i < freq.length; i++) {
				if (freq[i] != 0) {
					NodeElement temp = new NodeElement(i, freq[i]);
					pqueue.add(temp);
				}
			}
		}
				
			
		// Phase 2  -  Build Huffman Tree with MinHeap
		private NodeElement buildHuffmanTree(PriorityQueue<NodeElement> pqNode) {
			//While there's more than 1 element in the minHeap
			NodeElement tempParent;
			NodeElement left;
			NodeElement right;
			while(pqNode.size()>1) {
				
				//Put lesser value as left
				left = pqNode.poll();//System.out.println(" left read ");left.printLevelOrder();
				
				//put successor as right
				right = pqNode.poll();//System.out.println(" right read ");right.printLevelOrder();
				
				//Create new node by combining left + right values (occurences)
				tempParent = new NodeElement(-1, (left.getOccurence()+right.getOccurence()), left, right);
				//System.out.println(" Parent read ");//tempParent.printLevelOrder();
				
				
				//Adding Parent to MinHeap
				pqNode.add(tempParent);
				//System.out.println(" Moving to next Parent - Pq Size : "+ pqNode.size() + "\n");
				
			}
			
			return pqNode.poll();
		}



		//Will build prefixes for each character occuring at least once
		private void buildCodeMap(int[]freq, NodeElement root, HashMap<Integer, String> codes) {
			for (int i = 0; i < freq.length; i++) {
				if(freq[i] > 0)
					ifNodeExists(root, i, "",codes);
				}
		}
		

		//Modified Method That will build the prefixes for each char depending on their frequency
		//taken ( Inspired) from
		//https://www.geeksforgeeks.org/search-a-node-in-binary-tree/
			// Function to traverse the tree in preorder  and check if the given node exists in it  
		public boolean ifNodeExists( NodeElement node, int key, String prefix, HashMap<Integer, String> codeMapper ) { 
			
			if (node == null)  
			        return false;  
			  
			    if (node.getAsciiVal() == key) {
			    	//System.out.println("This is from id node when found");
//			    	System.out.println(prefix + " Prefix "+ prefix + " Lettre, ascii et occurrence "
//			    	+ Character.toString((char)node.getAsciiVal())
//			    	+" "+ node.getAsciiVal() + " "+ node.getOccurence());
			    	codeMapper.put(node.getAsciiVal(), prefix);
			    	return true;  
			    }
			  
			    // then recur on left subtree / 
			    boolean res1 = ifNodeExists(node.left, key, prefix +"0",codeMapper);  
			    if(res1) return true; // node found, no need to look further 
			  
			    // node is not found in left, so recur on right subtree / 
			    boolean res2 = ifNodeExists(node.right, key,prefix+"1",codeMapper);  
			  
			    return res2; 		
		}

		
		//Will build prefixes for each character occuring at least once
			private void buildCodeMap2(int[]freq, NodeElement root,HashMap<Integer, ArrayList<Integer>> intCodeMap) {
				for (int i = 0; i < freq.length; i++) {
					if(freq[i] > 0)
						ifNodeExists2(root, i,new ArrayList<Integer>(),intCodeMap);
					}
			}
			

			//Modified Method That will build the prefixes for each char depending on their frequency
			//Not entirely mine but did some adjustments
			//taken ( Inspired) from
			//https://www.geeksforgeeks.org/search-a-node-in-binary-tree/
				// Function to traverse the tree in preorder  and check if the given node exists in it  
			public boolean ifNodeExists2( NodeElement node, int key, ArrayList<Integer> prefixList,HashMap<Integer, ArrayList<Integer>> intCodeMap) { 
				
				if (node == null)  
				        return false;  
				  
				    if (node.getAsciiVal() == key) {
//				    	//System.out.println("This is from id node when found");
//				    	System.out.println(prefixList + " Prefix "+ prefixList + " Lettre, ascii et occurrence "
//				    	+ Character.toString((char)node.getAsciiVal())
//				    	+" "+ node.getAsciiVal() + " "+ node.getOccurence());
				    	intCodeMap.put(node.getAsciiVal(), new ArrayList<Integer>(prefixList));//OMG Do not put prefixList directly, :( make a new ArrayList
				    	return true;  
				    }
				  
				    // then recur on left subtree / 
				    prefixList.add(0);
				    boolean res1 = ifNodeExists2(node.left, key,prefixList,intCodeMap);
				    //prefixList cleanup after node is found 
				    prefixList.remove(prefixList.size()-1);
				    if(res1) return true; // node found, no need to look further 
				  
				    // node is not found in left, so recur on right subtree / 
				    prefixList.add(1);
				    boolean res2 = ifNodeExists2(node.right, key,prefixList,intCodeMap);
				    prefixList.remove(prefixList.size()-1); 
				  
				    return res2; 		
			}

		// ##### ------------------------------------------------------------------------------------------------ #####
		// ##### ------------------------------------------------------------------------------------------------ #####
		// ### START ##---------- Output files Methods (Mes essais)------------  Not Working Methods ----- ## START ###
		
			
			public void encodeFromStringBinary(String fileNameInput, String fileNameOutput) {

			try (
					// http://tutorials.jenkov.com/java-io/fileoutputstream.html
					// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce
					// qui augment nettement la performance.
					InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);)

			{

				BitOutputStream bitOup = new BitOutputStream(fileNameOutput);
				int byteRead;

			//showCodes();
			
			
			char[] charAr = null; StringBuilder fullWord=new StringBuilder();
				// Lis le fichier jusqu'� la fin
				System.out.println("Entering iputStream");
				while ((byteRead = inputStream.read()) != -1) {

			           	charAr = codesMap.get(byteRead).toCharArray();
		            	fullWord.append(codesMap.get(byteRead).toCharArray());
				}
				
				System.out.println("LENGTH OF Full " +fullWord.length());
				int maxindex = (fullWord.length()/8)*8;
				int j =8;
				String verifString ="";
				// Depending on length , go trough each 8 Characters
				for (int i = 0; i < maxindex; i+=8) {
					
//					System.out.print("de "+ i +" � " +j +" ");
//					System.out.println(fullWord.substring(i, j));
//					verifString+=fullWord.substring(i, j);
					bitOup.writeIntFromBinary(Integer.parseInt(fullWord.substring(i, j),2));
					j+=8;
				}
				bitOup.writeIntFromBinary(Integer.parseInt(fullWord.substring(maxindex),2));

				System.out.println(verifString);
				System.out.println("Last String " + fullWord.substring(maxindex));

				// Fermer le lecteur et le writer
				inputStream.close();
				bitOup.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}}
			
			
			
			
			
		
			public void showCodes() {
				String letterCode = "";
				String bigWord ="";
				String orig = "this is an example of a huffman tree";
				int moduloVal = 8;
				boolean a = false;
				for (char symbol : orig.toCharArray()) {
					letterCode += " "+symbol+ " ";
					for (int i : intListMap.get((int)symbol)) {
						letterCode += Integer.toString(i);
						bigWord += Integer.toString(i);
					}
				}

				
				String formatted ="";int j=8;
				for (int i = 0; i < 126; i+=8) {
					formatted += bigWord.substring(i,j);
					formatted += "  ";
					j+=8;
				}
				formatted += bigWord.substring(128);
			System.out.println("Will encode \n "+letterCode+" \n" + bigWord);
			System.out.println(formatted);
			
			}
		private void encodeFileFromListOfCodeMap(String fileNameInput, String fileNameOutput) {

			try (
					// http://tutorials.jenkov.com/java-io/fileoutputstream.html
					// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce
					// qui augment nettement la performance.
					InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);)

			{

				BitOutputStream bitOup = new BitOutputStream(fileNameOutput);
				int byteRead;

			showCodes();
				// Lis le fichier jusqu'� la fin
				System.out.println("Entering iputStream");
				while ((byteRead = inputStream.read()) != -1) {
					// Will get code in HashMap for character and write with buffer
					for (int b : intListMap.get(byteRead)) {
						bitOup.writeBit(b);
						
						//OMG @Andre - J'ai juste mis �a en commentaires, 
						// le temps est pass� de genre 20 minutes et plus (jlai ferm� pis c'�tait pas fini ,trop long) 
						// � genre 0,3 secondes
						// ...Bruhhhh (2 joursssss , twoooo f*#&%!� days mate
						
						// fullWord += Integer.toString(b);
						// System.out.println("Val of bit for symbol "+ byteRead + " " + b);
					}
				}

				// Fermer le lecteur et le writer
				inputStream.close();
				bitOup.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
			
			
			
			
		//Method that will Encode the data and write it to a file
		private void encodeFile1(String fileNameInput, String fileNameOutput) {
			// TODO Auto-generated method stub
			try (
					// http://tutorials.jenkov.com/java-io/fileoutputstream.html
					// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce qui augment nettement la performance.
					InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);
		            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(fileNameOutput),bufferSize);
					DataOutputStream dataOutput = new DataOutputStream(outputStream);)
			
			{
					
				BitOutputStream bitOup = new BitOutputStream(fileNameOutput);
	            //Voir l'algo, jsuis pas capble de l'expliquer.
	            int byteRead;
	            
	            String fullWord = "",bit=""; 
	            //Lis le fichier jusqu'� la fin
	            System.out.println("Entering iputStream");
	            while ((byteRead = inputStream.read()) != -1) {

	            	System.out.println("byteRead "+ byteRead + " Val " + Character.toString((char)byteRead));
	        			bit = codesMap.get(byteRead);
	        			if (bit != "1" || bit!="0") {
	        				System.err.println(bit + " Charac " + Character.toString((char)byteRead));
	        			}
	        			fullWord += bit;
	        			//dataOutput.writeInt(Integer.parseInt(bit));
	        			//dataOutput.writeByte(Integer.parseInt(bit));
	        			//dataOutput.writeChar(Integer.parseInt(bit));
	        			//outputStream.write(Integer.parseInt(bit));
	        			bitOup.writeBit(Integer.parseInt(bit));
	        		
	            }
	            System.out.println("Out of input Stream \n Entering TocharArray");
	            
//	            char[] charAr =  fullWord.toCharArray();
//	            for (char c : charAr) {
//					bitOup.writeBit(c);
//				}
	            
	            //dataOutput.writeInt(v);(fullWord.getBytes());
	            
	            //Fermer le lecteur et le writer
	            outputStream.close();
	            inputStream.close();
	            dataOutput.close();
	            bitOup.close();
		     } catch (IOException ex) {
		            ex.printStackTrace();
		     }
			
		}
		
		

		private void encodeFile2(String fileNameInput, String fileNameOutput) {
			try (
					// http://tutorials.jenkov.com/java-io/fileoutputstream.html
					// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce qui augment nettement la performance.
					InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);
		            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(fileNameOutput),bufferSize);
					DataOutputStream dataOutput = new DataOutputStream(outputStream);)
			
			{
					
				BitOutputStream bitOup = new BitOutputStream(fileNameOutput);
	            //Voir l'algo, jsuis pas capble de l'expliquer.
	            int byteRead;
	            
	            StringBuilder fullWord = new StringBuilder();String bit=""; 
	            //Lis le fichier jusqu'� la fin
	            System.out.println("Entering iputStream");
	            char[] charAr;
	            while ((byteRead = inputStream.read()) != -1) {

	            	//System.out.println("byteRead "+ byteRead + " Val " + Character.toString((char)byteRead));
	        			//bit = NodeElm.getHuffManCOde(root, byteRead, "");
	        			/*if (bit != "1" || bit!="0") {
	        				System.err.println(bit + " Charac " + Character.toString((char)byteRead));
	        			}*/
	            	charAr = codesMap.get(byteRead).toCharArray();
	            	fullWord.append(codesMap.get(byteRead).toCharArray());
	            	for ( char ch :fullWord.toString().toCharArray() )
	        			for (int i = 0; i < charAr.length; i++) {
							//bitOup.writeBit(Character.getNumericValue(charAr[i]));
						}
	        			//dataOutput.writeInt(Integer.parseInt(bit));
	        			//dataOutput.writeByte(Integer.parseInt(bit));
	        			//dataOutput.writeChar(Integer.parseInt(bit));
	        			//outputStream.write(Integer.parseInt(bit));
	        			//bitOup.writeBit(Integer.parseInt(bit));
	        		
	            }
	            System.out.println("Out of input Stream \n Entering TocharArray");
	            System.err.println("Full Word "+ fullWord.toString()+"\n");
	           charAr =  fullWord.toString().toCharArray();
	            /*for (char c : charAr) {
	            	if(c <0 || c > 1)
	            		System.err.println(c);
				}*/
	            
	            //dataOutput.write(fullWord.getBytes());
	            
	            //Fermer le lecteur et le writer
	            outputStream.close();
	            inputStream.close();
	            dataOutput.close();
	            bitOup.close();
		     } catch (IOException ex) {
		            ex.printStackTrace();
		     }		
		}


		private void encodeFile(String fileNameInput, String fileNameOutput) {
			
			String Output = "C:\\Users\\User\\Desktop\\Compress\\WriteInt.txt";
			String datFaOutput = "C:\\Users\\User\\Desktop\\Compress\\dataOutPut.txt";

			String bitOutput = "C:\\Users\\User\\Desktop\\Compress\\bitOutPut.txt";
			try (
					// http://tutorials.jenkov.com/java-io/fileoutputstream.html
					// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce qui augment nettement la performance.
		            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(Output),bufferSize);
					DataOutputStream dataOutput = new DataOutputStream(outputStream);)
			
			{
					
				BitOutputStream bitOup = new BitOutputStream(datFaOutput);
				String da = "110000011000101011110001010111010001111101100000010100100001001000111110010111011110101110001101111101110110010100011111110010110011011";
				
				int digi = 0 ,numdig =0;
//				for (int it : da.toCharArray()) {
//					System.out.println(it);
//					if(numdig == 8) {					
//						dataOutput.writeInt(digi);
//						numdig = 0;
//						digi = it - 0;
//						System.out.println(digi);
//						
//					}
//					else {
//						System.out.println(digi);
//						digi = it - 0;
//						System.err.println("After - " + digi);
	//
//						numdig++;
//						
//					}
//				}
				
				String dat = "110000011000101011110001010111010001111101100000010100100001001000111110010111011110101110001101111101110110010100011111110010110011011";
				int valint =0;
				String buildByte ="";
				for (int i = 0; i < dat.length(); i++) {
					while (buildByte.length()<8) {
						buildByte += dat.charAt(i);
					}
					System.out.println(buildByte);
					System.out.println(Integer.parseInt(buildByte));

				}
				
				for (char s : dat.toCharArray()) {
					if(buildByte.length() == 8) {
						System.out.println(buildByte);
						buildByte = "";
						
						if(buildByte.startsWith("0")) {
							System.out.println("hmm");
						}
						
						}
						buildByte += s;
				}
				
				
//				for (int iterable_element : dat.toCharArray()) {
//					bitOup.writeBit(Character.getNumericValue(iterable_element));
//				}
//				
				//Working
				//dataOutput.write(11000001);
				
				//outputStream.write("11000001".getBytes());
				

				//outputStream.write(da.getBytes());
				//dataOutput.writeBytes(da.getBytes());
									
	            outputStream.close();
	            dataOutput.close();
	            //bitOup.close();
		     } catch (IOException ex) {
		            ex.printStackTrace();
		     }
			
		}

		
		
		
		
		
		
		// ### END ##---------- Output files Methods (Mes essais)----------------  Not Working Methods ----- ## END ###
		// ##### ------------------------------------------------------------------------------------------------ #####
		// ##### ------------------------------------------------------------------------------------------------ #####
		
		
	/*-------------------------------------------------------------------------------------------------------------------------------
	 * -------------------------------------------------------------------------------------------------------------------------------
	 *------------------------------------------------------------------------------------------------------------------------------- 
	 * 	DECOMPRESIONN SECTION											DECOMPRESSION
	 * -------------------------------------------------------------------------------------------------------------------------------
	 */
		
		@Override
		public void decompress(String fileNameInput, String fileNameOutput) {
			// TODO Auto-generated method stub
			
			//#1 REad file to find maybe Frequency table 
			readFileToDecompress(fileNameInput);
			
			//#2 Build HuffMan Tree from FrequencyTable
			//NodeElement root = buildHuffManTreeFromFreqTable(frequTable);
			
			//#3 Read file from x to END and go to Huffman Tree with 0 = left and 1 = right and print if node is leaf
			
			
			//#4 OutPut file
		}
		
		private void readFileToDecompress(String fileNameInput) {
			try (
					// http://tutorials.jenkov.com/java-io/fileoutputstream.html
					// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce
					// qui augment nettement la performance.
					InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);)

			{

				int byteRead;
				String encodedData ="";
				// Lis le fichier jusqu'� la fin
				System.out.println("Entering iputStream");
				while ((byteRead = inputStream.read()) != -1) {
					System.out.print( "Encoded " + byteRead);
					System.out.print("  Char value : "+ (char)byteRead);
					System.out.print("  String value : "+ Character.toString((char)byteRead));
					System.out.println("  Int value : "+ (char)byteRead);
					System.out.println("BinaryValue " + Integer.toBinaryString(byteRead));
				}

				// Fermer le lecteur et le writer
				inputStream.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			
		}
		
		private NodeElement buildHuffManTreeFromFreqTable(int[] freqArray) {
			//Build MinHeap
			PriorityQueue<NodeElement> pqNode = new PriorityQueue<NodeElement>();
			buildMinHeap(frequTable, pqNode);
			//Return Huffman tree
			return buildHuffmanTree(pqNode);
		}
		
		
		
		// ##### ------------------------------------------------------------------------------------------------ #####
		// ##### ------------------------------------------------------------------------------------------------ #####
		// ##### START ------------------------ Decompression Methods   ----------------------------------- START #####
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		public void writeToFile(String fileNameInput) {
			try (
					// http://tutorials.jenkov.com/java-io/fileoutputstream.html
					// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce
					// qui augment nettement la performance.
					InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);)

			{
				String fileNameOutput = "C:\\Users\\User\\Desktop\\Compress\\Nyuaki_ex.txt";

				BitOutputStream bitOup = new BitOutputStream(fileNameOutput);
				int byteRead;
				String dat = "11000001";//1000101011110001010111";//010001111101100000010100100001001000111110010111011110101110001101111101110110010100011111110010110011011";
				IntStream a = dat.chars();
				int bitShift =0,count=0;
				for (char i : dat.toCharArray()) {
					System.out.print("char as is " + i +" ");
					System.out.print("char " + (char)i +" ");
					System.out.print("int " + (int)i +" ");
					System.out.print("val  " + (Character.toString((char)i)) +" ");
					System.out.print("Integer Parse "+ Integer.parseInt((Character.toString((char)i))));
					System.out.print(" Numerical Value " + Character.getNumericValue(i));
					
				     bitShift += i << count | i;

					System.out.println(" --- BitShift at count "+count +"  "+ bitShift);
					count++;
				}
				
				System.out.println("************************************************************************* \n \n");

				 bitShift =0;count=0;
				 int[]ar = new int[] {1,0,1,1,0,1,1};
				for (int i = 0; i <ar.length ; i++) {
					System.out.print("char as is " + ar[i] +" ");
					System.out.print("char " + (char)ar[i] +" ");
					System.out.print("int " + (int)ar[i] +" ");
					System.out.print("val  " + (Character.toString((char)ar[i])) +" ");
					System.out.print("Integer Parse "+ (Integer.toString((char)ar[i])));
					System.out.print(" Numerical Value " + Character.getNumericValue(ar[i]));
					
				     bitShift +=(ar[i] << count) | ar[i];

					System.out.print(" --- BitShift at count "+count +"  "+ bitShift);
					System.out.println(" --- BitShift as binary "+count +"  "+ Integer.toBinaryString(bitShift));

					count++;
				}
				// Fermer le lecteur et le writer
				inputStream.close();
				bitOup.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		
		
		
		
		
		
		
		/*
		I use a for loop to iterate the string and use charAt() to get each character to examine it. Since the String is implemented with an array, the charAt() method is a constant time operation.

		String s = "...stuff...";

		for (int i = 0; i < s.length(); i++){
		    char c = s.charAt(i);        
		    //Process char
		}
		That's what I would do. It seems the easiest to me.

		As far as correctness goes, I don't believe that exists here. It is all based on your personal style.*/
		
		public void binaryTest(String numb) {
			
			
			int[] arr = new int[8];
			int index =0;
			for (char i : numb.toCharArray()) {
				arr[index]=Character.getNumericValue(i);
						index++;
			}
			System.out.println(arr);
			
			String fullNumber="";
			int digits=0,numDigits =0;

			for (int i = 0; i < arr.length; i++) {
				
				fullNumber+=Integer.toString(arr[i]);
				
				System.out.print(" digits "+ digits + " numDigits "+ numDigits);
				System.out.print(" shifting "+ arr[i] +" will equal "+ (arr[i]<<numDigits));
			     digits += (arr[i] << numDigits) | digits;
			     numDigits++;
			     System.out.println(" digits after iteration  "+ digits + " fullnumber to date "+fullNumber);

			}
			System.out.println("Final digits value = "+digits  +" should be  "+Integer.parseInt(fullNumber, 2)
			+" But Real Real Value is " +Integer.parseInt(numb,2));
		}

		//Local tests
		public static void main(String[] args) {

			String fileNameInput = "C:\\Users\\User\\Desktop\\Compress\\ex.txt";
			String fileNameOutput = "C:\\Users\\User\\Desktop\\Compress\\joxe.txt";
			String jo = "C:\\Users\\User\\Desktop\\Compress\\Nayuki\\nayc.txt";
			String joawout = "C:\\Users\\User\\Desktop\\Compress\\joComString.txt";

			String mp3In = "C:\\Users\\User\\Desktop\\Compress\\kzb.mp3";
			String mp3Out = "C:\\Users\\User\\Desktop\\Compress\\kzbOut.mp3";
			
			HuffmanCompressorTest a = new HuffmanCompressorTest();
			//a.writeToFile(jo);
			
			//a.compress(fileNameInput, joawout);
			String read = "C:\\Users\\User\\Desktop\\Compress\\joxe.txt";
			a.readFileToDecompress(read);
			//a.decompress(joawout, null);
			
			//a.binaryTest("01000000");
			//a.decompress(fileNameInput, null);
//			a.decompress(fileNameOutput, fileNameOutput);
//			System.out.println("-------------------------------------\n Nyuaki");
//			a.decompress(fileNameInput, fileNameOutput);
//			System.out.println("-------------------------------------\n jo");
//			a.decompress(jo, fileNameOutput);
			// a.buildFrequencyTable();
		}

	}

	

