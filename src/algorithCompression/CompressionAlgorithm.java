package algorithCompression;

public interface CompressionAlgorithm {
	public void compress(String fileNameInput, String fileNameOutput);
	public void decompress(String fileNameInput, String fileNameOutput);
}
