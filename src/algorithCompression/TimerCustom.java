package algorithCompression;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class TimerCustom {
	private long startTime;
	
	public void start() {
		startTime = System.currentTimeMillis();
	}
	
	public void stop() {
        NumberFormat formatter = new DecimalFormat("#0.00000");
        System.out.println("The operation took : " + formatter.format((System.currentTimeMillis() - startTime) / 1000d) + " seconds");
        startTime = 0;
	}
}
