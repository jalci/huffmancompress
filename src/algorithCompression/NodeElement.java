package algorithCompression;

import java.io.PrintStream;

public class NodeElement implements Comparable<NodeElement> {

	public NodeElement right;
	public NodeElement left;
	private boolean isLeaf;
	private int occurence;
	private int asciiVal;
	
	
	
	
	//Constructor 1
	public NodeElement (int _asciiVal, int _occurence) {
		this.asciiVal = _asciiVal;
		this.occurence = _occurence;
		setIsLeaf(_asciiVal);
	}
	
	// Constructor 2
	public NodeElement (int _asciiVal, int _occurence, NodeElement leftChild, NodeElement rightChild) {
		this.asciiVal = _asciiVal;
		this.occurence = _occurence;
		this.right = rightChild;
		this.left = leftChild;
		setIsLeaf(_asciiVal);

	}
	
	// To determine if node is a leaf
	private void setIsLeaf(int _asciiVal) {
		if(_asciiVal != -1)
			this.isLeaf = true;
		else
			this.isLeaf = false;
	}
	
	//IsLeaf Getter
	public boolean isLeaf() {
		return this.isLeaf;
	}
	
	public int getOccurence() {
		return occurence;
	}


	public int getAsciiVal() {
		return asciiVal;
	}

	public void setAsciiVal(int asciiVal) {
		this.asciiVal = asciiVal;
	}
	
	
	
	@Override
	public int compareTo(NodeElement o) {
		if(this.occurence > o.getOccurence())  //MaxHeap : this < o ; MinHeap: this > o
			return 1;  
		else if(this.occurence < o.getOccurence())  
			return -1;  
		else if (this.asciiVal > o.getAsciiVal())//Tie breaker
			return 1;
		else if (this.asciiVal < o.getAsciiVal())
			return -1;
			else  
			return 0;
	}
    
	
	
	
	
	
	// UNUSED // UNRELATED TO LAB - Not My code, i copied it but it's just to show the tree
	//TREE VISUALISATION ------- NOT USED IN LAB, ONLY FOR VISUAL PRINTING OF TREE VALIDATION-----------------------------------------

	/* Given a binary tree. Print its nodes in level order 
    using array for implementing queue  */
   /*void printLevelOrder()  
   { 
       Queue<Node> queue = new LinkedList<Node>(); 
       queue.add(root); 
       while (!queue.isEmpty())  
       { 
 
           /* poll() removes the present head. 
           For more information on poll() visit  
           http://www.tutorialspoint.com/java/util/linkedlist_poll.htm 
           Node tempNode = queue.poll(); 
           System.out.print(tempNode.data + " "); 
 
           /*Enqueue left child 
           if (tempNode.left != null) { 
               queue.add(tempNode.left); 
           } 
 
           /*Enqueue right child 
           if (tempNode.right != null) { 
               queue.add(tempNode.right); 
           } 
       } 
   }*/ 
	
	
	public void traversePreOrder(StringBuilder sb, String padding, String pointer, NodeElement node) {
	    if (node != null) {
	        sb.append(padding);
	        sb.append(pointer);
	        sb.append(Character.toString((char)node.asciiVal));
	        sb.append("\n");
	 
	        StringBuilder paddingBuilder = new StringBuilder(padding);
	        paddingBuilder.append("|  ");
	 
	        String paddingForBoth = paddingBuilder.toString();
	        String pointerForRight = "|---";
	        String pointerForLeft = (node.right != null) ? "|--" : "L__";
	 
	        traversePreOrder(sb, paddingForBoth, pointerForLeft, node.left);
	        traversePreOrder(sb, paddingForBoth, pointerForRight, node.right);
	    }
	}
	
	public void print(PrintStream os) {
	    StringBuilder sb = new StringBuilder();
	    traversePreOrder(sb, "", "", this);
	    os.print(sb.toString());
	}


}
