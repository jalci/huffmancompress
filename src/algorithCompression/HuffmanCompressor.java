package algorithCompression;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.IntStream;
;



public class HuffmanCompressor implements CompressionAlgorithm {
	//int bufferSize = 20 * 1024;
	int bufferSize = 4 * 1024;
	private TimerCustom timer;
	private int[] frequTable;
	private HashMap<Integer, String> codesMap;
	private HashMap<Integer, ArrayList<Integer>> intListMap;
	//Testing Purpose
	private NodeElement root;
	
	public NodeElement getRoot() {
		return this.root;
	}
	
	
	
	public HuffmanCompressor() {
		timer = new TimerCustom();
	    codesMap = new HashMap<Integer, String>();
	    intListMap = new HashMap<Integer, ArrayList<Integer>>();

	}

	
	@Override
	public void compress(String fileNameInput, String fileNameOutput) {
		timer.start();

		//Build frequency table from input file
		frequTable = buildFrequencyTable(fileNameInput);
		
		//Create minHeap with frequencies
		PriorityQueue<NodeElement> pqNode = new PriorityQueue<NodeElement>();
		buildMinHeap(frequTable, pqNode);
		
		//Create Huffman tree
	    // Phase 2  - Build Huffman Tree
		NodeElement root = buildHuffmanTree(pqNode);
		
		//Delete
		this.root = root;
	    
		//Build a look-up table for each character's code
			//Build Int - String map
	    buildCodeMap(frequTable, root, codesMap);

	    	//Build Int - ArrayList map - Because of trouble writing in output file (char vs int)
	    buildCodeMap2(frequTable, root,intListMap);
	    

		//Phase 3 - Encode data to file
		//OutPut file
		//encodeFile1(fileNameInput,fileNameOutput);
		//encodeFile(fileNameInput,fileNameOutput);
		//encodeFile2(fileNameInput,fileNameOutput);
	    
	    //encodeFromStringBinary(fileNameInput, fileNameOutput);

	    
	    //Working
	    encodeFileFromListOfCodeMap(fileNameInput, fileNameOutput);
	    timer.stop();
	}
	




	// ##### ----------------------------- Compression Methods  --------------------------------------------#####

	
	// Will build frequency table from input file
	private int[] buildFrequencyTable(String fileNameInput) {
		int[] freq = new int[256];

		try (
				// http://tutorials.jenkov.com/java-io/fileoutputstream.html
				// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce
				// qui augmente nettement la performance.
				InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);) {

			int byteRead;

			// Lis le fichier jusqu'� la fin
			while ((byteRead = inputStream.read()) != -1) {
				freq[byteRead] += 1;
			}

			// Fermer le lecteur
			inputStream.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return freq;
	}

 
		
	//From frequencies, we build a minHeap that we'll use to create Huffman tree
	private void buildMinHeap(int[] freq, PriorityQueue<NodeElement> pqueue) {

		for (int i = 0; i < freq.length; i++) {
			if (freq[i] != 0) {
				NodeElement temp = new NodeElement(i, freq[i]);
				pqueue.add(temp);
			}
		}
	}
			
		
	// Phase 2  -  Build Huffman Tree with MinHeap
	private NodeElement buildHuffmanTree(PriorityQueue<NodeElement> pqNode) {
		//While there's more than 1 element in the minHeap
		NodeElement tempParent;
		NodeElement left;
		NodeElement right;
		while(pqNode.size()>1) {
			
			//Put lesser value as left
			left = pqNode.poll();//System.out.println(" left read ");left.printLevelOrder();
			
			//put successor as right
			right = pqNode.poll();//System.out.println(" right read ");right.printLevelOrder();
			
			//Create new node by combining left + right values (occurences)
			tempParent = new NodeElement(-1, (left.getOccurence()+right.getOccurence()), left, right);
			//System.out.println(" Parent read ");//tempParent.printLevelOrder();
			
			
			//Adding Parent to MinHeap
			pqNode.add(tempParent);
			//System.out.println(" Moving to next Parent - Pq Size : "+ pqNode.size() + "\n");
			
		}
		
		return pqNode.poll();
	}



	//Will build prefixes for each character occuring at least once
	private void buildCodeMap(int[]freq, NodeElement root, HashMap<Integer, String> codes) {
		for (int i = 0; i < freq.length; i++) {
			if(freq[i] > 0)
				ifNodeExists(root, i, "",codes);
			}
	}
	

	//Modified Method That will build the prefixes for each char depending on their frequency
	//taken ( Inspired) from
	//https://www.geeksforgeeks.org/search-a-node-in-binary-tree/
		// Function to traverse the tree in preorder  and check if the given node exists in it  
	public boolean ifNodeExists( NodeElement node, int key, String prefix, HashMap<Integer, String> codeMapper ) { 
		
		if (node == null)  
		        return false;  
		  
		    if (node.getAsciiVal() == key) {
		    	//System.out.println("This is from id node when found");
//		    	System.out.println(prefix + " Prefix "+ prefix + " Lettre, ascii et occurrence "
//		    	+ Character.toString((char)node.getAsciiVal())
//		    	+" "+ node.getAsciiVal() + " "+ node.getOccurence());
		    	codeMapper.put(node.getAsciiVal(), prefix);
		    	return true;  
		    }
		  
		    // then recur on left subtree / 
		    boolean res1 = ifNodeExists(node.left, key, prefix +"0",codeMapper);  
		    if(res1) return true; // node found, no need to look further 
		  
		    // node is not found in left, so recur on right subtree / 
		    boolean res2 = ifNodeExists(node.right, key,prefix+"1",codeMapper);  
		  
		    return res2; 		
	}

	
	//Will build prefixes for each character occuring at least once
		private void buildCodeMap2(int[]freq, NodeElement root,HashMap<Integer, ArrayList<Integer>> intCodeMap) {
			for (int i = 0; i < freq.length; i++) {
				if(freq[i] > 0)
					ifNodeExists2(root, i,new ArrayList<Integer>(),intCodeMap);
				}
		}
		

		//Modified Method That will build the prefixes for each char depending on their frequency
		//Not entirely mine but did some adjustments
		//taken ( Inspired) from
		//https://www.geeksforgeeks.org/search-a-node-in-binary-tree/
			// Function to traverse the tree in preorder  and check if the given node exists in it  
		public boolean ifNodeExists2( NodeElement node, int key, ArrayList<Integer> prefixList,HashMap<Integer, ArrayList<Integer>> intCodeMap) { 
			
			if (node == null)  
			        return false;  
			  
			    if (node.getAsciiVal() == key) {
//			    	//System.out.println("This is from id node when found");
//			    	System.out.println(prefixList + " Prefix "+ prefixList + " Lettre, ascii et occurrence "
//			    	+ Character.toString((char)node.getAsciiVal())
//			    	+" "+ node.getAsciiVal() + " "+ node.getOccurence());
			    	intCodeMap.put(node.getAsciiVal(), new ArrayList<Integer>(prefixList));//OMG Do not put prefixList directly, :( make a new ArrayList
			    	return true;  
			    }
			  
			    // then recur on left subtree / 
			    prefixList.add(0);
			    boolean res1 = ifNodeExists2(node.left, key,prefixList,intCodeMap);
			    //prefixList cleanup after node is found 
			    prefixList.remove(prefixList.size()-1);
			    if(res1) return true; // node found, no need to look further 
			  
			    // node is not found in left, so recur on right subtree / 
			    prefixList.add(1);
			    boolean res2 = ifNodeExists2(node.right, key,prefixList,intCodeMap);
			    prefixList.remove(prefixList.size()-1); 
			  
			    return res2; 		
		}

	// ##### ------------------------------------------------------------------------------------------------ #####
	// ##### ------------------------------------------------------------------------------------------------ #####
	// ### START ##---------- Output files Methods (Mes essais)------------  Not Working Methods ----- ## START ###
	
		//Build Big string containing  Encoded Data and write each 8 character of the string as a binary value
		public void encodeFromStringBinary(String fileNameInput, String fileNameOutput) {

		try (
				// http://tutorials.jenkov.com/java-io/fileoutputstream.html
				// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce
				// qui augment nettement la performance.
				InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);)

		{

			BitOutputStream bitOup = new BitOutputStream(fileNameOutput);
			int byteRead;

		//showCodes();
		
		
		char[] charAr = null; StringBuilder fullWord=new StringBuilder();
			// Lis le fichier jusqu'� la fin
			System.out.println("Entering iputStream");
			while ((byteRead = inputStream.read()) != -1) {

		           	charAr = codesMap.get(byteRead).toCharArray();
	            	fullWord.append(codesMap.get(byteRead).toCharArray());
			}
			
			System.out.println("LENGTH OF Full " +fullWord.length());
			int maxindex = (fullWord.length()/8)*8;
			int j =8;
			String verifString ="";
			// Depending on length , go trough each 8 Characters
			for (int i = 0; i < maxindex; i+=8) {
				
//				System.out.print("de "+ i +" � " +j +" ");
//				System.out.println(fullWord.substring(i, j));
//				verifString+=fullWord.substring(i, j);
				bitOup.writeIntFromBinary(Integer.parseInt(fullWord.substring(i, j),2));
				j+=8;
			}
			bitOup.writeIntFromBinary(Integer.parseInt(fullWord.substring(maxindex),2));

			System.out.println(verifString);
			System.out.println("Last String " + fullWord.substring(maxindex));

			// Fermer le lecteur et le writer
			inputStream.close();
			bitOup.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}}
		
		
		
		
		
	
		
	private void encodeFileFromListOfCodeMap(String fileNameInput, String fileNameOutput) {

		try (InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);)
		{

			BitOutputStream bitOup = new BitOutputStream(fileNameOutput);
			int byteRead;

			// Lis le fichier jusqu'� la fin
			System.out.println("Entering iputStream");
			while ((byteRead = inputStream.read()) != -1) {
				// Will get code in HashMap for character and write with buffer
				for (int b : intListMap.get(byteRead)) {
					bitOup.writeBits(b);
				}
			}

			// Fermer le lecteur et le writer
			inputStream.close();
			bitOup.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
		
		
		
		
	//Method that will Encode the data and write it to a file
	private void encodeFile1(String fileNameInput, String fileNameOutput) {
		// TODO Auto-generated method stub
		try (
				// http://tutorials.jenkov.com/java-io/fileoutputstream.html
				// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce qui augment nettement la performance.
				InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);
	            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(fileNameOutput),bufferSize);
				DataOutputStream dataOutput = new DataOutputStream(outputStream);)
		
		{
				
			BitOutputStream bitOup = new BitOutputStream(fileNameOutput);
            //Voir l'algo, jsuis pas capble de l'expliquer.
            int byteRead;
            
            String fullWord = "",bit=""; 
            //Lis le fichier jusqu'� la fin
            System.out.println("Entering iputStream");
            while ((byteRead = inputStream.read()) != -1) {

            	System.out.println("byteRead "+ byteRead + " Val " + Character.toString((char)byteRead));
        			bit = codesMap.get(byteRead);
        			if (bit != "1" || bit!="0") {
        				System.err.println(bit + " Charac " + Character.toString((char)byteRead));
        			}
        			fullWord += bit;
        			//dataOutput.writeInt(Integer.parseInt(bit));
        			//dataOutput.writeByte(Integer.parseInt(bit));
        			//dataOutput.writeChar(Integer.parseInt(bit));
        			//outputStream.write(Integer.parseInt(bit));
        			bitOup.writeBit(Integer.parseInt(bit));
        		
            }
            System.out.println("Out of input Stream \n Entering TocharArray");
            
//            char[] charAr =  fullWord.toCharArray();
//            for (char c : charAr) {
//				bitOup.writeBit(c);
//			}
            
            //dataOutput.writeInt(v);(fullWord.getBytes());
            
            //Fermer le lecteur et le writer
            outputStream.close();
            inputStream.close();
            dataOutput.close();
            bitOup.close();
	     } catch (IOException ex) {
	            ex.printStackTrace();
	     }
		
	}
	
	

	

	
	
	
	// ### END ##---------- Output files Methods (Mes essais)----------------  Not Working Methods ----- ## END ###
	// ##### ------------------------------------------------------------------------------------------------ #####
	// ##### ------------------------------------------------------------------------------------------------ #####
	
	
/*-------------------------------------------------------------------------------------------------------------------------------
 * -------------------------------------------------------------------------------------------------------------------------------
 *------------------------------------------------------------------------------------------------------------------------------- 
 * 	DECOMPRESIONN SECTION											DECOMPRESSION
 * -------------------------------------------------------------------------------------------------------------------------------
 */
	
	
	@Override
	public void decompress(String fileNameInput, String fileNameOutput) {
		// TODO Auto-generated method stub
		
		//#1 REad file to find maybe Frequency table 
		//readFileToDecompress(fileNameInput);
		
		//#2 Build HuffMan Tree from FrequencyTable
		//NodeElement root = buildHuffManTreeFromFreqTable(frequTable);
		
		//#3 Read file from x to END and go to Huffman Tree with 0 = left and 1 = right and print if node is leaf
		
		
		//#4 OutPut file
	}
	
	
	// ##### ------------------------------------------------------------------------------------------------ #####
	// ##### ------------------------------------------------------------------------------------------------ #####
	// ##### START ------------------------ Decompression Methods   ----------------------------------- START #####
	
	
	
	
	//When Reading back from encoded file this makes Sure that for small numbers 
	//like 7 -> 00000111, when converting from int to binary, we lose the leading zeros
	// "00000000".substring(Integer.toBinaryString(x).length())+(Integer.toBinaryString(x)));


	
	private NodeElement buildHuffManTreeFromFreqTable(int[] freqArray) {
		//Build MinHeap
		PriorityQueue<NodeElement> pqNode = new PriorityQueue<NodeElement>();
		buildMinHeap(frequTable, pqNode);
		//Return Huffman tree
		return buildHuffmanTree(pqNode);
	}
	
	
	

	//Local tests
	public static void main(String[] args) {

		String fileNameInput = "C:\\Users\\User\\Desktop\\Compress\\ex.txt";
		String fileNameOutput = "C:\\Users\\User\\Desktop\\Compress\\joxe.txt";
		String jo = "C:\\Users\\User\\Desktop\\Compress\\Nayuki\\nayc.txt";
		String joawout = "C:\\Users\\User\\Desktop\\Compress\\joComString.txt";

		String mp3In = "C:\\Users\\User\\Desktop\\Compress\\kzb.mp3";
		String mp3Out = "C:\\Users\\User\\Desktop\\Compress\\kzbOut.mp3";
		
		HuffmanCompressor a = new HuffmanCompressor();
		a.compress(fileNameInput, fileNameOutput);
		//a.compress(fileNameInput, fileNameOutput);
		//a.writeToFile(jo);
		
		//a.compress(fileNameInput, joawout);
		//String read = "C:\\Users\\User\\Desktop\\Compress\\Nayuki\\nayC.txt";
		//a.readFileToDecompress(read);
		//a.decompress(joawout, null);
		
		//a.binaryTest("01000000");
		//a.decompress(fileNameInput, null);
//		a.decompress(fileNameOutput, fileNameOutput);
//		System.out.println("-------------------------------------\n Nyuaki");
//		a.decompress(fileNameInput, fileNameOutput);
//		System.out.println("-------------------------------------\n jo");
//		a.decompress(jo, fileNameOutput);
		// a.buildFrequencyTable();
	}

}
