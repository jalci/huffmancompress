
package algorithCompression;


public class HuffmanDecoderTest {
	
	//Optional will probably delete
	private NodeElement root;
	
	public HuffmanDecoderTest(NodeElement _root) {
		
		this.root = _root;
	}
	
	//Optional will probably delete
	public void setRoot(NodeElement _root) {
		this.root = _root;
	}
	
	public String getStringEncodedData(String fileNameInput) {
		
		
		return null;
	}
	
	
	public void DecodeString(String encodedData) {
		
		NodeElement activeNode = root;
		NodeElement tempNode;
		
		//Tests
		String full ="";
		
		for (int i = 0; i < encodedData.length(); i++) {
//			System.out.print("Char at position " + i + " ");
//			System.out.println(encodedData.charAt(i));
			
			
			//Verify where to go
			if( Character.getNumericValue(encodedData.charAt(i))== 0)
				tempNode = activeNode.left;
			else
				tempNode = activeNode.right;
			
			//After getting Node, Verify if it's leaf
			//Print value and restart at root
			if(tempNode.isLeaf()) {
				System.out.println("Node found Ascii "+ tempNode.getAsciiVal() +" Letter "+ Character.toString((char)tempNode.getAsciiVal()));
				full += Character.toString((char)tempNode.getAsciiVal());
				activeNode = root;
			}
			else { //Else if not leaf, this becomes active node and we go through it's children
				activeNode = tempNode;
			}
				
		}
		System.out.println(full);
	}
	
	
	
	
	
	//Local Test for Tree walk
	/*
	 * a - 0
	 * f - 11
	 * t - 100
	 * h - 101
	 */
	
	public void buildRoot() {
		NodeElement one = new NodeElement(((int)"t".charAt(0)),1);
		NodeElement oneT = new NodeElement(((int)"h".charAt(0)),1);
		
		
		NodeElement parentTH = new NodeElement(-1,2);
		parentTH.left =one;parentTH.right = oneT;
		
		
		
		NodeElement twoF = new NodeElement(((int)"f".charAt(0)),2);
		
		NodeElement ThreeA = new NodeElement(((int)"a".charAt(0)),3);
		
		
		NodeElement PpTHF = new NodeElement(-1,(2+2),parentTH,twoF);
		
		
		root =  new NodeElement(-1, 7, ThreeA, PpTHF);
		
	}
	
	


	//Local scenario Tests
	public static void main(String[] args) {
		HuffmanDecoderTest dec = new HuffmanDecoderTest(null);
		System.out.println((int)"t".charAt(0));
		//dec.buildRoot();
		//dec.DecodeString("1001010110100");
		
		
		// #Scenario 1 - Compress File ex.txt -> compressedEx.txt 
		//Encoded String is already known, result should be 'this is an example of a huffman tree'
		String fileNameInput = "C:\\Users\\User\\Desktop\\Compress\\ex.txt";
		String fileNameOutput = "C:\\Users\\User\\Desktop\\Compress\\compressedEx.txt";
		
		HuffmanCompressor enc = new HuffmanCompressor();
		//Setting root of decoder
		enc.compress(fileNameInput, fileNameOutput);
		dec.setRoot(enc.getRoot());
		
		String encodedData ="110000111000000111110000001111010000011101100101010100110110101000111111010111011110101110011001001101110110010100000111110010111011011"; 
		dec.DecodeString(encodedData);
		
		
	}
}
