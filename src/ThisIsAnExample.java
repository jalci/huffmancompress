import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.Iterator;
import java.util.PriorityQueue;





public class ThisIsAnExample {

	String word = "this is an example of a huffman tree";
	int bufferSize = 20 * 1024;
	String fileNameInput = "C:\\Users\\User\\Desktop\\Compress\\ex.txt";
	//String fileNameInput = "C:\\Users\\User\\Desktop\\prot.mp4";


	private void buildFrequencyTableWithFile() {
		//Texte
		int [] freq = new int [256];
		
		TimerCustom timer = new TimerCustom();
		timer.start();
		//String fileNameInput = "C:\\Users\\User\\Desktop\\Islam.jpg";
		//String fileNameInput = "C:\\Users\\User\\Desktop\\huf.txt";
		

		try (
				// http://tutorials.jenkov.com/java-io/fileoutputstream.html
				// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce qui augment nettement la performance.
				InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);)
		{
				
				
            //Voir l'algo, jsuis pas capble de l'expliquer.
            //String symbol = Character.toString((char)inputStream.read());
            int byteRead;
            //String byteReadInString;
            
            //Lis le fichier jusqu'� la fin
            while ((byteRead = inputStream.read()) != -1) {
            	//System.out.println(byteRead + " byte Read");
            	freq[byteRead]+=1;
            }
            
            
        	/*//Add to minHeap
    		PriorityQueue<Element> prq = addToMinHeap(freq);
    		
    		// Display Min Heap
    	    System.out.println("Reading priotyQueue");
    	    System.out.println("peek " + prq.peek().occurence);
    	    while (!prq.isEmpty()) {
    	    	Element tem = prq.poll();
    	    	System.out.println( tem.asciiVal + " "+ Character.toString((char)tem.asciiVal) + " sorti  "+  tem.occurence +" fois" );
    	    }
            System.out.println("-----------------------\n"+freq.length);
    		timer.stop();*/
            
    		// ADDED AFTER SUCCESS
    		
    		// Building Via NodeElm
    		//Add to minHeap
    		PriorityQueue<NodeElm> pqNode = new PriorityQueue<NodeElm>();		// Building Via Node 
    		addNodeToMinHeap(freq, pqNode);
    		// or 
    		// PriorityQueue<NodeElm> pqNode = addNodeToMinHeap(freq); == Wich to choose ? Optimal Method ? Andre
    		System.out.println(pqNode.size() + " Size after void");

    	
    	    
            System.out.println("-----------------------\n Reading from Node ");
    	    System.out.println("Reading priotyQueue Element ");
    	    
    	  /*  System.out.println("peek " + pqNode.peek().occurence);
    	    while (!pqNode.isEmpty()) {
    	    	Element tem = pqNode.poll();
    	    	System.out.println( tem.asciiVal + " "+ Character.toString((char)tem.asciiVal) + " sorti  "+  tem.occurence +" fois" );
    	    }*/
    	    
    	    
    	    
    	    // Phase 2  - Build Huffman Tree
    	    PriorityQueue<NodeElm> tempQ = new PriorityQueue<NodeElm>();
    	    tempQ = pqNode;
    	    NodeElm root = buildHuffmanTree(pqNode);
    	    
    	    //root.printLevelOrder();
    	    System.out.println("The tree");
    	    NodeElm.initCodesMap();
    	    for (int i = 0; i < freq.length; i++) {
				if(freq[i] > 0)
					NodeElm.ifNodeExists(root, i, "");
			}
    	    //root.print(System.out);
    	    
    	    //Phase 3 Read or check if there
    	    System.out.println("Checking if there and reading \n \n");
    	    
    	   /* String fullWord = "";
    	    for (char i : word.toCharArray()) {
    			byteRead = (int)i;
    			fullWord += NodeElm.getHuffManCOde(root, byteRead, "");
    		}*/
    	    
    	    
    	    //Trying with file
    	    outPutFile(root);
    	    
    	    //System.out.println(fullWord);
    		timer.stop();
    		
    		
    		
            //Fermer le lecteur et le writer
            inputStream.close();
	     } catch (IOException ex) {
	            ex.printStackTrace();
	     }
		
		
		
		
		
		/*
		 * try { InputStream inputStream = new BufferedInputStream(new
		 * FileInputStream(fileNameInput), bufferSize); int byteRead; String
		 * byteReadInString;
		 * 
		 * //Lis le fichier jusqu'� la fin while ((byteRead = inputStream.read()) != -1)
		 * { byteReadInString = Character.toString((char) byteRead); } } catch
		 * (FileNotFoundException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
	}
	
	
	
	private void buildFrequencyTable() {
		int[] freq = new int[255];
		TimerCustom timer = new TimerCustom();
		timer.start();

		int byteRead = 0;		
		for (char i : word.toCharArray()) {
			byteRead = (int)i;
			System.out.println(i + " byte Read " + byteRead);
			freq[i] += 1;
		}
		
		
		/* PriorityQueue<Element> prq = addToMinHeap(freq); 				// Building Via Element
		 *  System.out.println("Reading priotyQueue Element ");
	    
	    System.out.println("peek " + prq.peek().occurence);
	    while (!prq.isEmpty()) {
	    	Element tem = prq.poll();
	    	System.out.println( tem.asciiVal + " "+ Character.toString((char)tem.asciiVal) + " sorti  "+  tem.occurence +" fois" );
	    }
	    */
		
		// Building Via NodeElm
		//Add to minHeap
		PriorityQueue<NodeElm> pqNode = new PriorityQueue<NodeElm>();		// Building Via Node 
		addNodeToMinHeap(freq, pqNode);
		// or 
		// PriorityQueue<NodeElm> pqNode = addNodeToMinHeap(freq); == Wich to choose ? Optimal Method ? Andre
		System.out.println(pqNode.size() + " Size after void");

	
	    
        System.out.println("-----------------------\n Reading from Node ");
	    System.out.println("Reading priotyQueue Element ");
	    
	  /*  System.out.println("peek " + pqNode.peek().occurence);
	    while (!pqNode.isEmpty()) {
	    	Element tem = pqNode.poll();
	    	System.out.println( tem.asciiVal + " "+ Character.toString((char)tem.asciiVal) + " sorti  "+  tem.occurence +" fois" );
	    }*/
	    
	    
	    
	    // Phase 2  - Build Huffman Tree
	    NodeElm root = buildHuffmanTree(pqNode);
	    
	    root.printLevelOrder();
	    
	    //Phase 3 Read or check if there
	    System.out.println("Checking if there and reading \n \n");
	    
	    String fullWord = "";
	    for (char i : word.toCharArray()) {
			byteRead = (int)i;
			//fullWord += NodeElm.getHuffManCOde(root, byteRead, "");
		}
	    
	    
	    //Trying with file
	    //outPutFile(root);
	    
	    System.out.println(fullWord);
		timer.stop();
		

	}
	
	
	
	


	//--------------------------------- Utility Methods ----------------------------------------
	public void readFreqTable(int[]freq) {
		for (int i = 0; i < freq.length; i++) {
			if(freq[i]!=0) {
				System.out.println(i +" "+ Character.toString((char)i) +" = "+freq[i]+" fois");
			}
		}
	}
	
	
	// Building MinHeap with Element
	public PriorityQueue<Element> addToMinHeap(int[]freq) {
		//PriorityQueue<Element> pqueue = new PriorityQueue<Element>(Collections.reverseOrder()); MinHeap vs MaxHeap
		PriorityQueue<Element> pqueue = new PriorityQueue<Element>();

		for (int i = 0; i < freq.length; i++) {
			if(freq[i]!=0) {
				Element temp = new Element(i,freq[i]);
				pqueue.add(temp);
			}
		}
		return pqueue;
	}
	
	
	// Building Minheap With  NodeElm
	public PriorityQueue<NodeElm> addNodeToMinHeap(int[]freq) {
		//PriorityQueue<NodeElm> pqueue = new PriorityQueue<NodeElm>(Collections.reverseOrder());// MinHeap vs MaxHeap
		PriorityQueue<NodeElm> pqueue = new PriorityQueue<NodeElm>();

		for (int i = 0; i < freq.length; i++) {
			if(freq[i]!=0) {
				NodeElm temp = new NodeElm(i,freq[i]);
				pqueue.add(temp);
			}
		}
		return pqueue;
	}


	
	public void outPutFile(NodeElm root) {
		//String fileNameInput = "C:\\Users\\User\\Desktop\\ex.txt";
		
		//String fileNameInput = "C:\\Users\\User\\Desktop\\exemple.txt";

		String fileNameOutput = "C:\\Users\\User\\Desktop\\Compress\\kzab8a.txt";

		try (
				// http://tutorials.jenkov.com/java-io/fileoutputstream.html
				// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce qui augment nettement la performance.
				InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);
	            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(fileNameOutput),bufferSize);
				DataOutputStream dataOutput = new DataOutputStream(outputStream);)
		
		{
				
			BitOutputStream bitOup = new BitOutputStream(fileNameOutput);
            //Voir l'algo, jsuis pas capble de l'expliquer.
            int byteRead;
            
            String fullWord = "",bit=""; 
            //Lis le fichier jusqu'� la fin
            System.out.println("Entering iputStream");
            while ((byteRead = inputStream.read()) != -1) {

            	System.out.println("byteRead "+ byteRead + " Val " + Character.toString((char)byteRead));
        			bit = NodeElm.charCodes.get(byteRead);
        			if (bit != "1" || bit!="0") {
        				System.err.println(bit + " Charac " + Character.toString((char)byteRead));
        			}
        			fullWord += bit;
        			//dataOutput.writeInt(Integer.parseInt(bit));
        			//dataOutput.writeByte(Integer.parseInt(bit));
        			//dataOutput.writeChar(Integer.parseInt(bit));
        			//outputStream.write(Integer.parseInt(bit));
        			bitOup.writeBit(Integer.parseInt(bit));
        		
            }
            System.out.println("Out of input Stream \n Entering TocharArray");
            
//            char[] charAr =  fullWord.toCharArray();
//            for (char c : charAr) {
//				bitOup.writeBit(c);
//			}
            
            //dataOutput.writeInt(v);(fullWord.getBytes());
            
            //Fermer le lecteur et le writer
            outputStream.close();
            inputStream.close();
            dataOutput.close();
            bitOup.close();
	     } catch (IOException ex) {
	            ex.printStackTrace();
	     }
	}
	
	
	// Optimisation  ? Pass PQ in param or return PQ 
	public void addNodeToMinHeap(int[]freq, PriorityQueue<NodeElm> pqueue ) {
		
		for (int i = 0; i < freq.length; i++) {
			if(freq[i]!=0) {
				NodeElm temp = new NodeElm(i,freq[i]);
				pqueue.add(temp);
			}
		}
		
	}
	
	// Phase 2  -  Build Huffman Tree with MinHeap
	private NodeElm buildHuffmanTree(PriorityQueue<NodeElm> pqNode) {
		//While there's more than 1 element in the minHeap
		NodeElm tempParent;
		NodeElm left;
		NodeElm right;
		while(pqNode.size()>1) {
			
			//Put lesser value as left
			left = pqNode.poll();//System.out.println(" left read ");left.printLevelOrder();
			
			//put successor as right
			right = pqNode.poll();//System.out.println(" right read ");right.printLevelOrder();
			
			//Create new node by combining left + right values (occurences)
			tempParent = new NodeElm(-1, (left.occurence+right.occurence), left, right);
			//System.out.println(" Parent read ");//tempParent.printLevelOrder();
			
			
			//Adding Parent to MinHeap
			pqNode.add(tempParent);
			//System.out.println(" Moving to next Parent - Pq Size : "+ pqNode.size() + "\n");
			
		}
		
		return pqNode.poll();
	}
	
	
	
	
	public static void main(String[] args) {
		
		
		ThisIsAnExample a = new ThisIsAnExample();
		a.buildFrequencyTableWithFile();
		//a.buildFrequencyTable();
	}
}



