import java.io.PrintStream;
import java.util.Comparator;
import java.util.HashMap;

public class NodeElm extends Element{

	public NodeElm right;
	public NodeElm left;
	private boolean isLeaf;
	public static HashMap<Integer, String> charCodes;
	
	
	public static void initCodesMap() {
		charCodes = new HashMap<Integer, String>();
	}
	
	//Constructor 1
	public NodeElm (int _asciiVal, int _occurence) {
		super(_asciiVal,_occurence);
		setIsLeaf(_asciiVal);
	}
	
	// Constructor 2
	public NodeElm (int _asciiVal, int _occurence, NodeElm leftChild, NodeElm rightChild) {
		super(_asciiVal,_occurence);
		this.right = rightChild;
		this.left = leftChild;
		setIsLeaf(_asciiVal);

	}
	
	// To determine if node is a leaf
	private void setIsLeaf(int _asciiVal) {
		if(_asciiVal != -1)
			this.isLeaf = true;
		else
			this.isLeaf = false;
	}
	
	//IsLeaf Getter
	public boolean isLeaf() {
		return this.isLeaf;
	}
	
	
	
	 /* function to print level order traversal of tree*/
    void printLevelOrder() 
    { 
        int h = height(this); 
        int i; 
        for (i=1; i<=h; i++) 
            printGivenLevel(this, i); 
    } 
	
	
    
    int height(NodeElm root) 
    { 
        if (root == null) 
           return 0; 
        else
        { 
            /* compute  height of each subtree */
            int lheight = height(root.left); 
            int rheight = height(root.right); 
              
            /* use the larger one */
            if (lheight > rheight) 
                return(lheight+1); 
            else return(rheight+1);  
        } 
    } 
    
    
    
    void printGivenLevel (NodeElm root ,int level) 
    { 
        if (root == null) 
            return; 
        if (level == 1) {
        	if(root.asciiVal == -1) {
        		System.out.println("Parent value - This is not a leaf. Value =  " + root.occurence + " Ascii "+ root.asciiVal);
        	}
        	else {
        	System.out.println( " String val " + Character.toString((char)root.asciiVal) + " Val " + root.asciiVal + " Occurence " + root.occurence);
        	}
        }
        else if (level > 1) 
        { 
            printGivenLevel(root.left, level-1); 
            printGivenLevel(root.right, level-1); 
        } 
    }


	public void readCode(String read) {
		// TODO Auto-generated method stub
		char[] digits1 = read.toCharArray();
		for (char c : digits1) {
		}
	} 
    
    
	
	
	//https://www.geeksforgeeks.org/search-a-node-in-binary-tree/
	// ReadNode
	// Function to traverse the tree in preorder  
	// and check if the given node exists in it  
	public static boolean ifNodeExists( NodeElm node, int key, String prefix )  
	{  
		//System.out.println( "Starting Prefix " +prefix);
		
	    if (node == null)  
	        return false;  
	  
	    if (node.asciiVal == key) {
	    	//System.out.println("This is from id node when found");
	    	System.out.println(prefix + " Prefix "+ prefix + " Lettre, ascii et occurrence "+ Character.toString((char)node.asciiVal)+" "+ node.asciiVal + " "+ node.occurence);
	    	charCodes.put(node.asciiVal, prefix);
	    	return true;  
	    }
	  
	    // then recur on left sutree / 
	    boolean res1 = ifNodeExists(node.left, key, prefix +"0");  
	    if(res1) return true; // node found, no need to look further 
	  
	    // node is not found in left, so recur on right subtree / 
	    boolean res2 = ifNodeExists(node.right, key,prefix+"1");  
	  
	    return res2; 
	} 
	
	
	
	//https://www.geeksforgeeks.org/search-a-node-in-binary-tree/
	// ReadNode
	// Function to traverse the tree in preorder  
	// and check if the given node exists in it  
	public static String getHuffManCOde( NodeElm node, int key, String prefix )  
	{  
	
			//System.out.println( "Starting Prefix " +prefix);
			
		    if (node == null)  
		        return prefix;  
		  
		    if (node.asciiVal == key) {
		    	//System.out.println("This is from id node when found");
		    	System.out.println(prefix + " Prefix "+ prefix + " Lettre, ascii et occurrence "+ Character.toString((char)node.asciiVal)+" "+ node.asciiVal + " "+ node.occurence);
		    	return prefix+"-xxz";  
		    }
		  
		    // then recur on left sutree / 
		    prefix = getHuffManCOde(node.left, key, prefix +"0");  
		    if(prefix.contains("-"))
		    	return prefix; // node found, no need to look further 
		  
		    // node is not found in left, so recur on right subtree / 
		     prefix = getHuffManCOde(node.right, key,prefix+"1");  
		  
		    return prefix; 
		
		
	}
	
	
    
    
	/* Given a binary tree. Print its nodes in level order 
    using array for implementing queue  */
   /*void printLevelOrder()  
   { 
       Queue<Node> queue = new LinkedList<Node>(); 
       queue.add(root); 
       while (!queue.isEmpty())  
       { 
 
           /* poll() removes the present head. 
           For more information on poll() visit  
           http://www.tutorialspoint.com/java/util/linkedlist_poll.htm 
           Node tempNode = queue.poll(); 
           System.out.print(tempNode.data + " "); 
 
           /*Enqueue left child 
           if (tempNode.left != null) { 
               queue.add(tempNode.left); 
           } 
 
           /*Enqueue right child 
           if (tempNode.right != null) { 
               queue.add(tempNode.right); 
           } 
       } 
   }*/ 
	
	
	//TRee Visualisation ----------------------------------------------------------------------
	public void traversePreOrder(StringBuilder sb, String padding, String pointer, NodeElm node) {
	    if (node != null) {
	        sb.append(padding);
	        sb.append(pointer);
	        sb.append(Character.toString((char)node.asciiVal));
	        sb.append("\n");
	 
	        StringBuilder paddingBuilder = new StringBuilder(padding);
	        paddingBuilder.append("|  ");
	 
	        String paddingForBoth = paddingBuilder.toString();
	        String pointerForRight = "|---";
	        String pointerForLeft = (node.right != null) ? "|--" : "L__";
	 
	        traversePreOrder(sb, paddingForBoth, pointerForLeft, node.left);
	        traversePreOrder(sb, paddingForBoth, pointerForRight, node.right);
	    }
	}
	
	public void print(PrintStream os) {
	    StringBuilder sb = new StringBuilder();
	    traversePreOrder(sb, "", "", this);
	    os.print(sb.toString());
	}
}
