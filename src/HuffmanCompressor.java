

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.PriorityQueue;
;



public class HuffmanCompressor {
	int bufferSize = 20 * 1024;
	private TimerCustom timer;
	private int[] frequTable;
	private HashMap<Integer, String> codesMap;
	
	public HuffmanCompressor() {
		timer = new TimerCustom();
	    codesMap = new HashMap<Integer, String>();

	}

	
	
	public void compress(String fileNameInput, String fileNameOutput) {
		timer.start();

		//Build frequency table from input file
		frequTable = buildFrequencyTable(fileNameInput);
		
		//Create minHeap with frequencies
		PriorityQueue<NodeElm> pqNode = new PriorityQueue<NodeElm>();
		buildMinHeap(frequTable, pqNode);
		
		
		
		//Create Huffman tree
	    // Phase 2  - Build Huffman Tree
		NodeElm root = buildHuffmanTree(pqNode);
	    
		//Build a look-up table for each character's code
	    buildCodeMap(frequTable, root, codesMap);
	    
		
		//Phase 3 - Encode data to file
		//OutPut file
		//encodeFile1(fileNameInput,fileNameOutput);
		encodeFile(fileNameInput,fileNameOutput);
		encodeFile2(fileNameInput,fileNameOutput);
	    
	    timer.stop();
	}
	




	// ##### ----------------------------- Compression Methods  --------------------------------------------#####

	
	// Will build frequency table from input file
	private int[] buildFrequencyTable(String fileNameInput) {
		int[] freq = new int[256];

		try (
				// http://tutorials.jenkov.com/java-io/fileoutputstream.html
				// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce
				// qui augmente nettement la performance.
				InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);) {

			int byteRead;

			// Lis le fichier jusqu'� la fin
			while ((byteRead = inputStream.read()) != -1) {
				freq[byteRead] += 1;
			}

			// Fermer le lecteur
			inputStream.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return freq;
	}

 
		
	//From frequencies, we build a minHeap that we'll use to create Huffman tree
	private void buildMinHeap(int[] freq, PriorityQueue<NodeElm> pqueue) {

		for (int i = 0; i < freq.length; i++) {
			if (freq[i] != 0) {
				NodeElm temp = new NodeElm(i, freq[i]);
				pqueue.add(temp);
			}
		}
	}
			
		
	// Phase 2  -  Build Huffman Tree with MinHeap
	private NodeElm buildHuffmanTree(PriorityQueue<NodeElm> pqNode) {
		//While there's more than 1 element in the minHeap
		NodeElm tempParent;
		NodeElm left;
		NodeElm right;
		while(pqNode.size()>1) {
			
			//Put lesser value as left
			left = pqNode.poll();//System.out.println(" left read ");left.printLevelOrder();
			
			//put successor as right
			right = pqNode.poll();//System.out.println(" right read ");right.printLevelOrder();
			
			//Create new node by combining left + right values (occurences)
			tempParent = new NodeElm(-1, (left.getOccurence()+right.getOccurence()), left, right);
			//System.out.println(" Parent read ");//tempParent.printLevelOrder();
			
			
			//Adding Parent to MinHeap
			pqNode.add(tempParent);
			//System.out.println(" Moving to next Parent - Pq Size : "+ pqNode.size() + "\n");
			
		}
		
		return pqNode.poll();
	}



	//Will build prefixes for each character occuring at least once
	private void buildCodeMap(int[]freq, NodeElm root, HashMap<Integer, String> codes) {
		for (int i = 0; i < freq.length; i++) {
			if(freq[i] > 0)
				ifNodeExists(root, i, "",codes);
			}
	}
	

	//Modified Method That will build the prefixes for each char depending on their frequency
	//taken ( Inspired) from
	//https://www.geeksforgeeks.org/search-a-node-in-binary-tree/
		// Function to traverse the tree in preorder  and check if the given node exists in it  
	public boolean ifNodeExists( NodeElm node, int key, String prefix, HashMap<Integer, String> codeMapper ) { 
		
		if (node == null)  
		        return false;  
		  
		    if (node.getAsciiVal() == key) {
		    	//System.out.println("This is from id node when found");
		    	System.out.println(prefix + " Prefix "+ prefix + " Lettre, ascii et occurrence "
		    	+ Character.toString((char)node.getAsciiVal())
		    	+" "+ node.getAsciiVal() + " "+ node.getOccurence());
		    	codeMapper.put(node.getAsciiVal(), prefix);
		    	return true;  
		    }
		  
		    // then recur on left subtree / 
		    boolean res1 = ifNodeExists(node.left, key, prefix +"0",codeMapper);  
		    if(res1) return true; // node found, no need to look further 
		  
		    // node is not found in left, so recur on right subtree / 
		    boolean res2 = ifNodeExists(node.right, key,prefix+"1",codeMapper);  
		  
		    return res2; 		
	}


	// ##### ------------------------------------------------------------------------------------------------ #####
	// ##### ------------------------------------------------------------------------------------------------ #####
	// ### START ##---------- Output files Methods (Mes essais)------------  Not Working Methods ----- ## START ###
	
	
	
	//Method that will Encode the data and write it to a file
	private void encodeFile1(String fileNameInput, String fileNameOutput) {
		// TODO Auto-generated method stub
		try (
				// http://tutorials.jenkov.com/java-io/fileoutputstream.html
				// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce qui augment nettement la performance.
				InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);
	            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(fileNameOutput),bufferSize);
				DataOutputStream dataOutput = new DataOutputStream(outputStream);)
		
		{
				
			BitOutputStream bitOup = new BitOutputStream(fileNameOutput);
            //Voir l'algo, jsuis pas capble de l'expliquer.
            int byteRead;
            
            String fullWord = "",bit=""; 
            //Lis le fichier jusqu'� la fin
            System.out.println("Entering iputStream");
            while ((byteRead = inputStream.read()) != -1) {

            	System.out.println("byteRead "+ byteRead + " Val " + Character.toString((char)byteRead));
        			bit = codesMap.get(byteRead);
        			if (bit != "1" || bit!="0") {
        				System.err.println(bit + " Charac " + Character.toString((char)byteRead));
        			}
        			fullWord += bit;
        			//dataOutput.writeInt(Integer.parseInt(bit));
        			//dataOutput.writeByte(Integer.parseInt(bit));
        			//dataOutput.writeChar(Integer.parseInt(bit));
        			//outputStream.write(Integer.parseInt(bit));
        			bitOup.writeBit(Integer.parseInt(bit));
        		
            }
            System.out.println("Out of input Stream \n Entering TocharArray");
            
//            char[] charAr =  fullWord.toCharArray();
//            for (char c : charAr) {
//				bitOup.writeBit(c);
//			}
            
            //dataOutput.writeInt(v);(fullWord.getBytes());
            
            //Fermer le lecteur et le writer
            outputStream.close();
            inputStream.close();
            dataOutput.close();
            bitOup.close();
	     } catch (IOException ex) {
	            ex.printStackTrace();
	     }
		
	}
	
	

	private void encodeFile2(String fileNameInput, String fileNameOutput) {
		try (
				// http://tutorials.jenkov.com/java-io/fileoutputstream.html
				// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce qui augment nettement la performance.
				InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);
	            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(fileNameOutput),bufferSize);
				DataOutputStream dataOutput = new DataOutputStream(outputStream);)
		
		{
				
			BitOutputStream bitOup = new BitOutputStream(fileNameOutput);
            //Voir l'algo, jsuis pas capble de l'expliquer.
            int byteRead;
            
            StringBuilder fullWord = new StringBuilder();String bit=""; 
            //Lis le fichier jusqu'� la fin
            System.out.println("Entering iputStream");
            char[] charAr;
            while ((byteRead = inputStream.read()) != -1) {

            	//System.out.println("byteRead "+ byteRead + " Val " + Character.toString((char)byteRead));
        			//bit = NodeElm.getHuffManCOde(root, byteRead, "");
        			/*if (bit != "1" || bit!="0") {
        				System.err.println(bit + " Charac " + Character.toString((char)byteRead));
        			}*/
            	charAr = codesMap.get(byteRead).toCharArray();
            	fullWord.append(codesMap.get(byteRead).toCharArray());
            	for ( char ch :fullWord.toString().toCharArray() )
        			for (int i = 0; i < charAr.length; i++) {
						//bitOup.writeBit(Character.getNumericValue(charAr[i]));
					}
        			//dataOutput.writeInt(Integer.parseInt(bit));
        			//dataOutput.writeByte(Integer.parseInt(bit));
        			//dataOutput.writeChar(Integer.parseInt(bit));
        			//outputStream.write(Integer.parseInt(bit));
        			//bitOup.writeBit(Integer.parseInt(bit));
        		
            }
            System.out.println("Out of input Stream \n Entering TocharArray");
            System.err.println("Full Word "+ fullWord.toString()+"\n");
           charAr =  fullWord.toString().toCharArray();
            /*for (char c : charAr) {
            	if(c <0 || c > 1)
            		System.err.println(c);
			}*/
            
            //dataOutput.write(fullWord.getBytes());
            
            //Fermer le lecteur et le writer
            outputStream.close();
            inputStream.close();
            dataOutput.close();
            bitOup.close();
	     } catch (IOException ex) {
	            ex.printStackTrace();
	     }		
	}


	private void encodeFile(String fileNameInput, String fileNameOutput) {
		// TODO Auto-generated method stub
		
		String Output = "C:\\Users\\User\\Desktop\\Compress\\WriteInt.txt";
		String datFaOutput = "C:\\Users\\User\\Desktop\\Compress\\dataOutPut.txt";

		String bitOutput = "C:\\Users\\User\\Desktop\\Compress\\bitOutPut.txt";
		try (
				// http://tutorials.jenkov.com/java-io/fileoutputstream.html
				// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce qui augment nettement la performance.
	            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(Output),bufferSize);
				DataOutputStream dataOutput = new DataOutputStream(outputStream);)
		
		{
				
			BitOutputStream bitOup = new BitOutputStream(datFaOutput);
			String da = "110000011000101011110001010111010001111101100000010100100001001000111110010111011110101110001101111101110110010100011111110010110011011";
			
			int digi = 0 ,numdig =0;
//			for (int it : da.toCharArray()) {
//				System.out.println(it);
//				if(numdig == 8) {					
//					dataOutput.writeInt(digi);
//					numdig = 0;
//					digi = it - 0;
//					System.out.println(digi);
//					
//				}
//				else {
//					System.out.println(digi);
//					digi = it - 0;
//					System.err.println("After - " + digi);
//
//					numdig++;
//					
//				}
//			}
			
			String dat = "110000011000101011110001010111010001111101100000010100100001001000111110010111011110101110001101111101110110010100011111110010110011011";
			int valint =0;
			String buildByte ="";
			for (int i = 0; i < dat.length(); i++) {
				while (buildByte.length()<8) {
					buildByte += dat.charAt(i);
				}
				System.out.println(buildByte);
				System.out.println(Integer.parseInt(buildByte));

			}
			
			for (char s : dat.toCharArray()) {
				if(buildByte.length() == 8) {
					System.out.println(buildByte);
					buildByte = "";
					
					if(buildByte.startsWith("0")) {
						System.out.println("hmm");
					}
					
					}
					buildByte += s;
			}
			
			
//			for (int iterable_element : dat.toCharArray()) {
//				bitOup.writeBit(Character.getNumericValue(iterable_element));
//			}
//			
			//Working
			//dataOutput.write(11000001);
			
			//outputStream.write("11000001".getBytes());
			

			//outputStream.write(da.getBytes());
			//dataOutput.writeBytes(da.getBytes());
								
            outputStream.close();
            dataOutput.close();
            //bitOup.close();
	     } catch (IOException ex) {
	            ex.printStackTrace();
	     }
		
	}

	
	
	
	
	
	
	// ### END ##---------- Output files Methods (Mes essais)----------------  Not Working Methods ----- ## END ###
	// ##### ------------------------------------------------------------------------------------------------ #####
	// ##### ------------------------------------------------------------------------------------------------ #####
	
	
/*-------------------------------------------------------------------------------------------------------------------------------
 * -------------------------------------------------------------------------------------------------------------------------------
 *------------------------------------------------------------------------------------------------------------------------------- 
 * 	DECOMPRESIONN SECTION											DECOMPRESSION
 * -------------------------------------------------------------------------------------------------------------------------------
 */
	
	public void decompress(String fileNameInput, String fileNameOutput) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	// ##### ------------------------------------------------------------------------------------------------ #####
	// ##### ------------------------------------------------------------------------------------------------ #####
	// ##### START ------------------------ Decompression Methods   ----------------------------------- START #####
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	



	public static void main(String[] args) {

		String fileNameInput = "C:\\Users\\User\\Desktop\\Compress\\exemple.txt";
		String fileNameOutput = "C:\\Users\\User\\Desktop\\Compress\\Propre.txt";

		HuffmanCompressor a = new HuffmanCompressor();
		a.compress(fileNameInput, fileNameOutput);
		// a.buildFrequencyTable();
	}

}