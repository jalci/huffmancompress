import java.util.Comparator;

public class Element implements Comparable<Element>{
	int occurence;
	int asciiVal;
	
	
	public Element (int _asciiVal, int _occurence) {
		this.asciiVal = _asciiVal;
		this.occurence = _occurence;
	}
	
	
//	@Override
//	public int compare(Element o1, Element o2) {
//		// TODO Auto-generated method stub
//		return  o1.occurence - o2.occurence;
//	}
	
	
	public int getOccurence() {
		return occurence;
	}


	public int getAsciiVal() {
		return asciiVal;
	}

	public void setAsciiVal(int asciiVal) {
		this.asciiVal = asciiVal;
	}


	@Override
	public int compareTo(Element o) {
		if(this.occurence==o.getOccurence())  
			return 0;  
			else if(this.occurence > o.getOccurence())  //MaxHeap : this < o ; MinHeap: this > o
			return 1;  
			else  
			return -1;
	}
}
