
	import java.io.BufferedInputStream;
	import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
	import java.io.DataInputStream;
	import java.io.DataOutputStream;
	import java.io.FileInputStream;
	import java.io.FileOutputStream;
	import java.io.IOException;
	import java.io.InputStream;
	import java.io.OutputStream;
	import java.util.HashMap;
	import java.util.Map;

	public class LzwCompressor {
		
		String fileNameInput = "C:\\Users\\User\\Desktop\\Compress\\ex.txt";
		String test = "this is an example of a huffman tree";
		
		// http://tutorials.jenkov.com/java-io/fileoutputstream.html
		// Utiliser pour am�liorer la performance quand on �crit dans le fichier. Voir URL.
		int bufferSize = 20 * 1024;
		private HashMap<String, Integer> lzwStringDict;
		private HashMap<Integer, String> lzwShortDict;
		private TimerCustom timer;
		
		public LzwCompressor() {
			timer = new TimerCustom();
			System.out.println(test.codePoints());
			
				
				System.out.println(test.codePointCount(0,34));
		

		}
		
		private void initializeDictionnary() {
			lzwStringDict = new HashMap<String, Integer>();
			lzwShortDict = new HashMap<Integer, String>();
			// Initialize the dictionary with the first 256 ASCII characters.
			for (int i = 0; i < 256; i ++) {
				lzwStringDict.put(Character.toString((char)i), (int) i);
				lzwShortDict.put((int)i, Character.toString((char)i));
			}
		}
		
		public void compress(String fileNameInput, String fileNameOutput) {
			initializeDictionnary();
			//Permet de conna�tre la performance du programme.
			timer.start();
			
			try (
					// http://tutorials.jenkov.com/java-io/fileoutputstream.html
					// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce qui augment nettement la performance.
					InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);
		            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(fileNameOutput),bufferSize);
					DataOutputStream dataOutput = new DataOutputStream(outputStream);)
			{
					
					
	            //Voir l'algo, jsuis pas capble de l'expliquer.
	            String symbol = Character.toString((char)inputStream.read());
	            int byteRead;
	            String byteReadInString;
	            
	            //Lis le fichier jusqu'� la fin
	            while ((byteRead = inputStream.read()) != -1) {
	            	byteReadInString = Character.toString((char) byteRead);
	            	
	            	//Si le dictionnaire contient la concat�nation du symbol et du dernier byte lue, on met la concat�nation
	            	//dans la variable "symbol" et on lie le prochain byte.
	            	if(lzwStringDict.containsKey(symbol + byteReadInString)) {
	            		symbol += byteReadInString;
	            		
	            	//Sinon, on ajoute la concat�nation dans le dictionnaire et la variable "symbole" est reinitialiser pour ne contenir
	            	//que la valeur du dernier byte lue. On �crit dans le fichier la valeur int de la concat�nation dans le dictionnaire.
	            	} else {
	            		dataOutput.writeInt(lzwStringDict.get(symbol));
	            		lzwStringDict.put(symbol + byteReadInString,(int) lzwStringDict.size());
	                	
	                	//Ajoute le dernier byte lue
	                	symbol = byteReadInString;
	            	}
	            }
	            dataOutput.writeInt(lzwStringDict.get(symbol.toString()));
	            
	            //Fermer le lecteur et le writer
	            outputStream.close();
	            inputStream.close();
	            dataOutput.close();
		     } catch (IOException ex) {
		            ex.printStackTrace();
		     }
			
			timer.stop();
		}

		
		public void decompress(String fileNameInput, String fileNameOutput) {
			initializeDictionnary();
			timer.start();
			try (
					// http://tutorials.jenkov.com/java-io/fileoutputstream.html
					// En utilisant un BufferedInputStream, on �vite de lire les bytes 1 par 1. Ce qui augment nettement la performance.
					InputStream inputStream = new BufferedInputStream(new FileInputStream(fileNameInput), bufferSize);
					DataInputStream dataInput = new DataInputStream(inputStream);
		            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(fileNameOutput), bufferSize);)	{
				
	            //Voir l'algo, jsuis pas capble de l'expliquer.
	            String s = null;
	            
	            //Contiendra le dernier byte lue
	            int k;
	            
	            //Lis le fichier jusqu'� la fin
	            //Voir l'algorigthe, pas sur comment l'expliquer.
	            while (dataInput.available() > 0) {
	            	k = dataInput.readInt();
	            	String seq = null;
	            	
	            	if (lzwShortDict.containsKey(k)) {
	            		seq = lzwShortDict.get(k);
	            	}
	            		
	            	if(seq == null) {
	            		seq = s + s.substring(0,1);
	            	} 
	            	
	            	//On s�pare le string qui se trouve dans notre HashMap en plusieurs caract�res pour retrouver leur �quivalent
	            	//en byte.
	            	char[] letters = seq.toCharArray();
	        		
	        		for(char c : letters) {
	        			outputStream.write((int) c);
	        		}
	        		
	            	if(s != null) {
	            		
	            		lzwShortDict.put((int)lzwShortDict.size(),s + seq.substring(0, 1));
	            	}
	            	
	            	s = seq;
	            }
	            
	    		timer.stop();
	    		
	            //Fermer le lecteur et le writer
	            outputStream.close();
	            inputStream.close();
	            dataInput.close();
	            
		     } catch (IOException ex) {
		            ex.printStackTrace();
		     }
		}
		
		public static void main(String[] args) {
			// TODO Auto-generated method stub
		LzwCompressor lz = new LzwCompressor();
		//lz.compress(fileNameInput, fileNameOutput);
		
		}
	}



